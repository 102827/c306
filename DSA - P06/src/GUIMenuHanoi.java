import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class GUIMenuHanoi {

	private static String[] menuItem = { "Start New Game", "Move Disc" };

	private static Tower hanoi[] = new Tower[3];
	private static Disc pile[] = new Disc[4];

	private static void doOption(int choice) {

		switch (choice) {
		case 0:
			// TODO: P07 Task 10 - Create Three Towers for hanoi[] array
			hanoi[0] = new Tower("Tower 1");
			hanoi[1] = new Tower("Tower 2");
			hanoi[2] = new Tower("Tower 3");
			// TODO: P07 Task 11 - Assign Four Discs for pile[] array
			pile[0] = Disc.SIZE2;
			pile[1] = Disc.SIZE3;
			pile[2] = Disc.SIZE4;
			pile[3] = Disc.SIZE5;
			// TODO: P07 Task 12 - Place the Four Discs on the First Tower
			// Need a For loop
			for (int i = 3; i >= 0; i--) {
				hanoi[0].placeDisc(pile[i]);
			}
			joinTowers();
			//displayTowers();
			break;

		case 1:
			int startTower = GUIKeyboard.readInt("From (1,2,3) : ") - 1;
			int destTower = GUIKeyboard.readInt("To   (1,2,3) : ") - 1;

			if (startTower < 0 || startTower > 2 || destTower < 0
					|| destTower > 2) {
				GUIKeyboard.display("Invalid Tower.");
				break;
			}

			// TODO: P07 Task 13 - Implement the application logic
			Disc startTowerDisc = hanoi[startTower].topDisc();
			if (startTowerDisc == null) {
				GUIKeyboard.display("No disc to move.");
			} else {
				if (hanoi[destTower].placeDisc(startTowerDisc)) {
					hanoi[startTower].removeDisc();
				} else {
					GUIKeyboard.display("Invalid Move");
				}
				joinTowers();
				//displayTowers();
			}
			break;

		}
	}

	private static boolean advanced = false;

	private static void displayTowers() {
		GUIMenuHanoi.clear();

		for (int i = 0; i < 3; i++) {
			output(hanoi[i]);
		}
	}
	
	private static String[] splitTowerString(Tower abc)
	{
		String[] lines = abc.toString().split("\n");
		return lines;
	}
	
	private static void joinTowers()
	{
		listModel.clear();
		String[] tower1 = splitTowerString(hanoi[0]);
		String[] tower2 = splitTowerString(hanoi[1]);
		String[] tower3 = splitTowerString(hanoi[2]);
		for (int i = 0; i < tower1.length; i++) {
			listModel.addElement(tower1[i] + " " +  tower2[i] + " " + tower3[i]);
		}
	}
	
	
	public static void main(String[] args) {
		GUIMenuHanoi.showMenu("Tower of Hanoi");
		
	}

	//
	// DO NOT CHANGE ANY CODE FROM THIS POINT ONWARDS
	// UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
	//
	private static JFrame win;
	private static DefaultListModel listModel = new DefaultListModel();
	private static JList jlist = new JList(listModel);
	private static JPanel pnl1 = new JPanel(new GridLayout(menuItem.length, 1,
			5, 5));

	public static void showMenu(String title) {
		win = new JFrame(title);
		win.setBounds(100, 100, 600, 400);

		// Create the Menu Option Buttons
		pnl1.setBorder(BorderFactory.createTitledBorder("Menu Items"));
		for (int i = 0; i < menuItem.length; i++) {
			JButton btn = new JButton(menuItem[i]);
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String command = e.getActionCommand();
					for (int i = 0; i < menuItem.length; i++) {
						if (command.equals(menuItem[i])) {
							doOption(i);
							break;
						}
					}
				}
			});
			pnl1.add(btn);
		}
		win.add(pnl1, BorderLayout.NORTH);

		// Create the Display Area
		JPanel p2 = new JPanel(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(jlist);
		p2.setBorder(BorderFactory.createTitledBorder("Display Area"));
		p2.add(scrollPane, BorderLayout.CENTER);
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));

		JPanel p3 = new JPanel(new FlowLayout());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
			}
		});

		p3.add(btnClear);
		p2.add(p3, BorderLayout.SOUTH);
		win.add(p2, BorderLayout.CENTER);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);
	}

	public static void output(String line) {
		String[] lines = line.split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}

	public static void output(Object obj) {
		String[] lines = obj.toString().split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}

	public static void clear() {
		listModel.clear();
	}
}
