public enum Disc {
	SIZE5('&', 5),
	SIZE4('#', 4),
	SIZE3('m', 3),
	SIZE2('@', 2);

	private int radius;
	private char pattern;
	private static int MAXRADIUS = 5;

	private Disc(char p, int r) {
		pattern = p;
		radius = r;
	}

	public int getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		String disc = "|";

		for (int i = 0; i < radius; i++) {
			disc = pattern + disc + pattern;
		}

		for (int i = 0; i < MAXRADIUS - radius; i++) {
			disc = ' ' + disc + ' ';
		}

		return disc;
	}

}
