public class Tower {
	private Stack pile;
	private static int MAXHEIGHT = 5;

	public Tower(String title) {
		pile = new Stack(title);
	}

	public Disc topDisc() {
		if (!pile.isEmpty()) {
			return pile.peek();
		} else {
			return null;
		}
	}

	public boolean placeDisc(Disc disc) {
		// TODO: P07 Task 9 - Implement placeDisc
		if (pile.isEmpty()) {
			pile.push(disc);
			return true;
		} else {
			if (pile.size() < 5) {
				if (topDisc().getRadius() > disc.getRadius()) {
					pile.push(disc);
					return true;
				}
				return false;
			}
			return false;
		}
	}

	public boolean removeDisc() {
		if(!pile.isEmpty())
		{
			pile.pop();
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-11s", pile.getTitle()) + "\n");
		for (int i = 0; i < MAXHEIGHT - pile.size(); i++) {
			sb.append("     |     \n");
		}
		for (Disc d : pile.getList()) {
			sb.append(d.toString() + "\n");
		}
		sb.append("===========\n\n");
		return sb.toString();
	}

}
