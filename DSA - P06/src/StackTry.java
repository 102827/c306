import java.util.Stack;

public class StackTry {
   public static void main(String[] args) {
      Stack<Character> stack1 = new Stack<Character>();
      Stack<Character> stack2 = new Stack<Character>();
      Stack<Character> stack3 = new Stack<Character>();

      // TODO: P07 Task 6 - Create Stack 1
      //           A. Push four times
      stack1.push('A');
      stack1.push('B');
      stack1.push('X');
      stack1.push('Y');


      // DONE: Create Stack 2
      stack2.push('Z');

      // TODO: P07 Task 7 - Create Stack 3
      stack3.push('B');
      stack3.push('C');
      stack3.push('A');


      // Display Stack 1,2,3
      System.out.println("B E F O R E");
      System.out.println(stack1);
      System.out.println(stack2);
      System.out.println(stack3);

      // TODO: P07 Task 8 - Using Push and Pop to move items from stacks
      //
      
      stack2.push(stack1.pop());
      stack2.push(stack3.pop());
      stack3.push(stack1.pop());
      stack2.push(stack1.pop());
      stack2.push(stack3.pop());
      stack2.push(stack1.pop());
      stack1.push(stack3.pop());
      stack1.push(stack2.pop());
      stack3.push(stack2.pop());
      stack3.push(stack2.pop());

      // Display Stack 1, 2, 3
      System.out.println("A F T E R");
      System.out.println(stack1);
      System.out.println(stack2);
      System.out.println(stack3);

   }



}
