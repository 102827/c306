import java.util.ArrayList;

public class Queue {
	public static final int MAX = 10;

	private ArrayList<Player> list;

	public Queue() {
		list = new ArrayList<Player>();
	}

	public boolean enqueue(Player p) {
		if (!isFull()) {
			list.add(p);
			return true;
		}
		return false;
	}

	public Player dequeue() {
		if (!isEmpty()) {
			return list.remove(0);
		}
		return null;
	}

	public Player peek() {
		if (!isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	public int size() {
		return list.size();
	}

	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		}
		return false;
	}

	public boolean isFull() {
		if(size()>=MAX)
		{
			return true;
		}
		return false;
	}

	public ArrayList<Player> getList() {
		return list;
	}

	@Override
	public String toString() {
		String line = "";
		for (Player p : list) {
			line = line + p.getName() + " ";
		}
		return line;
	}
}
