import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class GameServerTest {
	private GameServer server;
	private Player player1, player2;

	@Before
	public void setUp() throws Exception {
		server = new GameServer();
		player1 = new Player("player1");
		player2 = new Player("player2");
	}

	@Test
	public void testIsEmptyServerEmpty() {
		assertThat(server.isServerEmpty(), is(true));
	}

	@Test
	public void testIsServerEmpty() {
		server.logon(player1);

		assertThat(server.isServerEmpty(), is(false));
	}

	@Test
	public void testIsEmptyServerFull() {
		assertThat(server.isServerFull(), is(false));
	}

	@Test
	public void testIsServerFull() {
		for (int i = 0; i < GameServer.MAX_PLAYERS; i++) {
			server.logon(player2);
		}

		assertThat(server.isServerFull(), is(true));
	}

	@Test
	public void testLogonOnePlayer() {
		server.logon(player1);

		List<Player> players = server.getPlayers();
		assertThat(players.size(), is(1));
		assertThat(players, hasItem(player1));
	}

	@Test
	public void testLogonTwoPlayers() {
		server.logon(player2);
		server.logon(player1);

		List<Player> players = server.getPlayers();
		assertThat(players.size(), is(2));
		assertThat(players, hasItem(player1));
		assertThat(players, hasItem(player2));
	}

	@Test
	public void testLogoutEmptyServer() {
		server.logout();

		assertThat(server.isServerEmpty(), is(true));
	}

	@Test
	public void testLogon() {
		server.logon(player2);
		server.logon(player1);

		server.logout();

		assertThat(server.getPlayers().size(), is(1));
	}

	@Test
	public void testGetPlayersEmptyServer() {
		assertThat(server.getPlayers(), is(Collections.EMPTY_LIST));
	}

}
