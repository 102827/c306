import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutDemo2 {

	public GridLayoutDemo2() {

		JFrame frame1 = new JFrame("Grid Layout");
		frame1.setBounds(0, 0, 350, 250);

		frame1.setLayout(new GridLayout(2, 2));

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();

		panel1.setBorder(BorderFactory.createTitledBorder("Panel A"));
		panel2.setBorder(BorderFactory.createTitledBorder("Panel B"));
		panel3.setBorder(BorderFactory.createTitledBorder("Panel C"));
		panel4.setBorder(BorderFactory.createTitledBorder("Panel D"));

		panel1.setLayout(new BorderLayout());
		panel2.setLayout(new FlowLayout());
		panel3.setLayout(new BorderLayout());
		panel4.setLayout(new GridLayout(2,2));
		
		panel1.add(new JButton("A1"),BorderLayout.NORTH);
		panel1.add(new JButton("A2"),BorderLayout.CENTER);
		panel1.add(new JButton("A3"),BorderLayout.SOUTH);

		panel2.add(new JButton("B1"));
		panel2.add(new JButton("B2"));
		panel2.add(new JButton("B3"));

		panel3.add(new JButton("C1"),BorderLayout.WEST);
		panel3.add(new JButton("C2"),BorderLayout.CENTER);
		panel3.add(new JButton("C3"),BorderLayout.EAST);

		panel4.add(new JButton("D1"));
		panel4.add(new JButton("D2"));
		panel4.add(new JButton("D3"));

		frame1.add(panel1);
		frame1.add(panel2);
		frame1.add(panel3);
		frame1.add(panel4);

		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setVisible(true);

	}

	public static void main(String[] args) {
		new GridLayoutDemo2();
	}

}
