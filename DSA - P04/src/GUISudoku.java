import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class GUISudoku {
	int SIZE = 9;
	Sudoku game = new Sudoku();
	Integer numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	JComboBox<Integer> valueBox = new JComboBox<Integer>(numbers);
	JTextField rowTF = new JTextField();
	JTextField colTF = new JTextField();
	JTable table;
	JLabel messageLabel = new JLabel("Game started!");
	Random random = new Random();
	JPanel inputPanel = new JPanel();
	JPanel rowPanel = new JPanel();
	JPanel colPanel = new JPanel();
	JPanel valuePanel = new JPanel();
	JPanel enterPanel = new JPanel();
	JPanel displayPanel = new JPanel();
	JPanel messagePanel = new JPanel();
	
	public static void main(String[] args) {
		new GUISudoku();
	}

	public GUISudoku() {
		JFrame mainFrame = new JFrame("Sudoku");
		mainFrame.setBounds(0, 0, 400, 300);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainFrame.add(inputPanel(), BorderLayout.NORTH);
		mainFrame.add(displayPanel(), BorderLayout.CENTER);
		mainFrame.add(messagePanel(), BorderLayout.SOUTH);

		mainFrame.setVisible(true);
	}

	private Component inputPanel() {
		
		inputPanel.setLayout(new GridLayout(2, 2));
		inputPanel.setBorder(new TitledBorder("Enter Position"));
		

		JLabel rowLabel = new JLabel("Row ");
		JLabel colLabel = new JLabel("Column ");
		JLabel valueLabel = new JLabel("Value ");

		JButton enterBtn = new JButton("Enter");
		enterBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				onClickEnterBtn();
			}
		});
		rowTF.setColumns(2);
		rowTF.setBackground(Color.YELLOW);
		colTF.setColumns(2);
		colTF.setBackground(Color.YELLOW);
		rowPanel.add(rowLabel);
		rowPanel.add(rowTF);
		colPanel.add(colLabel);
		colPanel.add(colTF);
		valuePanel.add(valueLabel);
		valuePanel.add(valueBox);
		enterPanel.add(enterBtn);

		inputPanel.add(rowPanel);
		inputPanel.add(colPanel);
		inputPanel.add(valuePanel);
		inputPanel.add(enterPanel);

		return inputPanel;
	}

	private Component displayPanel() {
		

		table = new JTable(9, 9) {
			public Component prepareRenderer(TableCellRenderer renderer,
					int row, int column) {

				Component c = super.prepareRenderer(renderer, row, column);

				return c;
			}

			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		table.setPreferredSize(new Dimension(300, 150));
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				table.setValueAt("?", row, col);
			}
		}
		table.setShowGrid(false);
		displayPanel.add(table);
		return displayPanel;
	}

	private Component messagePanel() {
		messagePanel.add(messageLabel);
		return messagePanel;
	}

	public void onClickEnterBtn() {

		int row = Integer.parseInt(rowTF.getText());
		int col = Integer.parseInt(colTF.getText());
		int valueInt = numbers[valueBox.getSelectedIndex()];
		String valueString = valueBox.getSelectedItem().toString();
		game.setValue(valueInt, row, col);

		if (game.isValid()) {
			messageLabel.setText("Game Completed");
		}

		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		for (int rowS = 0; rowS < SIZE; rowS++) {
			for (int colS = 0; colS < SIZE; colS++) {
				String newValue = game.getValue(rowS, colS);
				int r = random.nextInt(100) + 155;// 155~255
				int g = random.nextInt(100) + 155;// 155~255
				int b = random.nextInt(100) + 155;// 155~255
				renderer.setBackground(new Color(r, g, b));
				inputPanel.setBackground(new Color(r,g,b));
				rowPanel.setBackground(new Color(r,g,b));
				colPanel.setBackground(new Color(r,g,b));
				enterPanel.setBackground(new Color(r,g,b));
				valuePanel.setBackground(new Color(r,g,b));
				displayPanel.setBackground(new Color(r,g,b));
				messagePanel.setBackground(new Color(r,g,b));
				table.getColumnModel().getColumn(colS)
						.setCellRenderer(renderer);
				table.setValueAt(newValue, rowS, colS);
			}
		}
	}
}