public class Sudoku {
	public static final int SIZE = 9;

	private int grid[][];

	public Sudoku() {
		grid = new int[SIZE][SIZE];
	}

	public String getValue(int row, int col) {
		if (row >= SIZE || row < 0 || col >= SIZE || col < 0) {
			throw new IllegalArgumentException();
		}

		int value = grid[row][col];
		if (value == 0) {
			return "?";
		}
		return Integer.toString(value);
	}

	public void setValue(int value, int row, int col) {
		if (row >= SIZE || row < 0 || col >= SIZE || col < 0) {
			throw new IllegalArgumentException();
		}
		grid[row][col] = value;
	}

	public boolean isArrayValid(int array[]) {
		int[] counter = countNumbers(array);

		return checkIfAllNumbersAreOne(counter);
	}

	private boolean checkIfAllNumbersAreOne(int[] counter) {
		boolean result = true;
		for (int i = 0; i < counter.length; i++) {
			int j = counter[i];

			if (j != 1) {
				result = false;
				break;
			}
		}

		return result;
	}

	private int[] countNumbers(int[] array) {
		int[] counter = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				int j = array[i] - 1;

				counter[j] += 1;
			}
		}
		return counter;
	}

	public boolean isValid() {
		boolean result = isAllRowsValid();

		if (result) {
			result = isAllColsValid();
		}
		if (result) {
			result = is3x3valid();
		}

		return result;
	}

	protected boolean isAllColsValid() {
		boolean result = true;

		for (int col = 0; col < SIZE; col++) {
			int[] tmp = extractCol(grid, col);

			if (!isArrayValid(tmp)) {
				result = false;
				break;
			}
		}
		return result;
	}

	protected int[] extractCol(int[][] array, int col) {
		int[] tmp = new int[SIZE];

		for (int row = 0; row < SIZE; row++) {
			int num = array[row][col];
			tmp[row] = num;
		}

		return tmp;
	}

	protected boolean isAllRowsValid() {
		boolean result = true;

		for (int i = 0; i < SIZE; i++) {
			if (!isArrayValid(grid[i])) {
				result = false;
				break;
			}
		}
		return result;
	}

	protected int[] returnValidRow() {
		int[] validRow = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for (int i = 0; i < SIZE; i++) {
			if (isArrayValid(grid[i])) {
				validRow[i] = 1;
			}
		}

		return validRow;
	}

	protected int[] returnValidCol() {
		int[] validCol = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for (int col = 0; col < SIZE; col++) {
			int[] tmp = extractCol(grid, col);
			if (isArrayValid(grid[col])) {
				validCol[col] = 1;
			}
		}

		return validCol;
	}

	protected boolean is3x3valid() {
		for (int i = 0; i < SIZE; i += 3) {
			for (int j = 0; j < SIZE; j += 3) {
				if (!isArrayValid(extract3x3(grid, i, j))) {
					return false;
				}
			}
		}
		return true;
	}

	protected int[] extract3x3(int[][] array, int row, int col) {
		int[] tmp = new int[SIZE];
		int count = 0;
		for (int i = row; i < row + 3; i++) {
			for (int j = col; j < col + 3; j++) {
				tmp[count] = array[i][j];
				count++;
			}
		}

		return tmp;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				result.append(grid[i][j] + " ");
			}
			result.append("\n");
		}

		return result.toString();
	}
}
