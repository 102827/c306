public class FourInLineBoard {

	public static final int ROWS = 6;
	public static final int COLS = 7;
	private Move[][] board;

	public FourInLineBoard() {
		board = new Move[ROWS][COLS];
		newGame();
	}

	public void newGame() {
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS; c++) {
				board[r][c] = Move.EMPTY;
			}
		}

	}

	public void placeMove(int column, Move player) {
		for (int r = ROWS - 1; r >= 0; r--) {
			if (board[r][column] == Move.EMPTY) {
				board[r][column] = player;
				break;
			}
		}

	}

	public Move seeMove(int row, int column) {
		return board[row][column];
	}

	public boolean checkWin(Move player) {
		boolean result = false;
		if (checkVertical(player) || checkHorizontal(player)
				|| checkBackwardDiagonal(player)
				|| checkForwardDiagonal(player)) {
			result = true;
		}
		return result;
	}

	public boolean checkVertical(Move player) {
		for (int r = 0; r < ROWS - 3; r++) {
			for (int c = 0; c < COLS; c++) {
				if ((board[r][c] == player) && (board[r + 1][c] == player)
						&& (board[r + 2][c] == player)
						&& (board[r + 3][c] == player)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkHorizontal(Move player) {
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS - 3; c++) {
				if ((board[r][c] == player) && (board[r][c + 1] == player)
						&& (board[r][c + 2] == player)
						&& (board[r][c + 3] == player)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkForwardDiagonal(Move player) {
		for (int r = 0; r < ROWS - 3; r++) {
			for (int c = 3; c < COLS - 1; c++) {
				if ((board[r][c] == player) && (board[r + 1][c - 1] == player)
						&& (board[r + 2][c - 2] == player)
						&& (board[r + 3][c - 3] == player)) {
					return true;
				}
			}
			System.out.println();
		}
		return false;
	}

	public boolean checkBackwardDiagonal(Move player) {
		for (int r = 0; r < ROWS - 3; r++) {
			for (int c = 0; c < COLS - 3; c++) {
				if ((board[r][c] == player) && (board[r + 1][c + 1] == player)
						&& (board[r + 2][c + 2] == player)
						&& (board[r + 3][c + 3] == player)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isColFull(int col) {
		if (board[0][col] != Move.EMPTY) {
			return true;
		}
		return false;
	}

	public boolean isBoardFull() {
		int totalGrid = 6 * 7;
		int counter = 0;
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS; c++) {
				if (board[r][c] != Move.EMPTY) {
					counter++;
				}
			}
		}
		if (counter == totalGrid) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLS; c++) {
				builder.append(board[r][c]);
			}
			builder.append("\n");
		}
		return builder.toString();
	}
}
