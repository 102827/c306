public class Patterns {
	public static void main(String args[]) {
		System.out.println("Pattern A");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print("% ");
			}
			System.out.println();
		}
		
		
		System.out.println();
		System.out.println("Pattern B");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5 - i; j++) {
				System.out.print("% ");
			}
			System.out.println();
		}
		
		
		System.out.println();
		System.out.println("Pattern C");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print("% ");
			}
			System.out.println();
		}
		
		
		System.out.println();
		System.out.println("Pattern D");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (j == i) {
					System.out.print("% ");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();
		}
		
		
		System.out.println();
		System.out.println("Pattern E");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (j % 2 == i % 2) {
					System.out.print("% ");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
