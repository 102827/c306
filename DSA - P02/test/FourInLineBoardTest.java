import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class FourInLineBoardTest {
	private FourInLineBoard board;
	private Random random;

	@Before
	public void setup() {
		board = new FourInLineBoard();
		random = new Random();
	}

	@Test
	public void testNewBoard() {
		for (int i = 0; i < FourInLineBoard.ROWS; i++) {
			for (int j = 0; j < FourInLineBoard.COLS; j++) {
				assertThat(board.seeMove(i, j), is(Move.EMPTY));
			}
		}
	}

	@Test
	public void testPlaceFirstMove() {
		int col = random.nextInt(FourInLineBoard.COLS);
		board.placeMove(col, Move.RED);

		assertThat(board.seeMove(FourInLineBoard.ROWS - 1, col), is(Move.RED));
	}

	@Test
	public void testPlaceMoveOnTop() {
		int col = random.nextInt(FourInLineBoard.COLS);
		board.placeMove(col, Move.RED);
		board.placeMove(col, Move.YELLOW);

		assertThat(board.seeMove(FourInLineBoard.ROWS - 2, col),
				is(Move.YELLOW));
	}

	@Test
	public void testHorizontalWin() {
		Move move = Move.RED;
		assertThat(board.checkHorizontal(move), is(false));

		board.placeMove(1, move);
		board.placeMove(2, move);
		board.placeMove(3, move);
		board.placeMove(4, move);

		assertThat(board.checkHorizontal(move), is(true));
		assertThat(board.checkWin(move), is(true));
	}

	@Test
	public void testNoWinOnEmptyBoard() {
		Move move = Move.RED;
		assertThat(board.checkHorizontal(move), is(false));
		assertThat(board.checkVertical(move), is(false));
		assertThat(board.checkForwardDiagonal(move), is(false));
		assertThat(board.checkBackwardDiagonal(move), is(false));
		assertThat(board.checkWin(move), is(false));
	}

	@Test
	public void testVerticalWin() {
		Move move = Move.RED;

		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);

		assertThat(board.checkVertical(move), is(true));
		assertThat(board.checkWin(move), is(true));
	}

	@Test
	public void testForwardDiagonalWin() {
		Move move = Move.RED;
		Move other = Move.YELLOW;

		board.placeMove(2, move);

		board.placeMove(3, other);
		board.placeMove(3, move);

		board.placeMove(4, other);
		board.placeMove(4, other);
		board.placeMove(4, move);

		board.placeMove(5, other);
		board.placeMove(5, other);
		board.placeMove(5, other);
		board.placeMove(5, move);

		assertThat(board.checkForwardDiagonal(move), is(true));
		assertThat(board.checkWin(move), is(true));
	}

	@Test
	public void testBackwardDiagonalWin() {
		Move move = Move.RED;
		Move other = Move.YELLOW;

		board.placeMove(2, other);
		board.placeMove(2, other);
		board.placeMove(2, other);
		board.placeMove(2, move);

		board.placeMove(3, other);
		board.placeMove(3, other);
		board.placeMove(3, move);

		board.placeMove(4, other);
		board.placeMove(4, move);

		board.placeMove(5, move);

		assertThat(board.checkBackwardDiagonal(move), is(true));
		assertThat(board.checkWin(move), is(true));
	}

	@Test
	public void testIsBoardFull() {
		Move move = Move.RED;
		Move other = Move.YELLOW;

		board.placeMove(0, other);
		board.placeMove(0, other);
		board.placeMove(0, other);
		board.placeMove(0, move);
		board.placeMove(0, other);
		board.placeMove(0, other);
		board.placeMove(0, other);

		board.placeMove(1, other);
		board.placeMove(1, other);
		board.placeMove(1, other);
		board.placeMove(1, move);
		board.placeMove(1, other);
		board.placeMove(1, other);
		board.placeMove(1, other);

		board.placeMove(2, other);
		board.placeMove(2, other);
		board.placeMove(2, other);
		board.placeMove(2, move);
		board.placeMove(2, other);
		board.placeMove(2, other);
		board.placeMove(2, other);

		board.placeMove(3, other);
		board.placeMove(3, other);
		board.placeMove(3, move);
		board.placeMove(3, other);
		board.placeMove(3, other);
		board.placeMove(3, move);
		board.placeMove(3, other);

		board.placeMove(4, other);
		board.placeMove(4, move);
		board.placeMove(4, other);
		board.placeMove(4, move);
		board.placeMove(4, other);
		board.placeMove(4, move);
		board.placeMove(4, other);
		board.placeMove(4, move);

		board.placeMove(5, move);
		board.placeMove(5, move);
		board.placeMove(5, move);
		board.placeMove(5, move);
		board.placeMove(5, move);
		board.placeMove(5, move);
		board.placeMove(5, move);

		board.placeMove(6, move);
		board.placeMove(6, move);
		board.placeMove(6, move);
		board.placeMove(6, move);
		board.placeMove(6, move);
		board.placeMove(6, move);
		board.placeMove(6, move);

		assertThat(board.isBoardFull(), is(true));
	}

	@Test
	public void testIsColFull() {
		Move move = Move.RED;

		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);
		board.placeMove(2, move);

		assertThat(board.isColFull(2), is(true));
	}
}
