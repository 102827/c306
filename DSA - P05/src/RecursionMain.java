public class RecursionMain {

	public int factorial_r(int num) {
		if (num == 1) {
			return 1;
		}

		return num * factorial_r(num - 1);
	}

	public int factorial_i(int num) {
		int ans = 1;
		for(int i=num;i>0;i--)
		{
			ans *= i;
		}
		return ans;
		
	}
	
	public static void main(String[] args) {
		RecursionMain rm = new RecursionMain();
		GUIKeyboard.display("Using Recursion, Factorial 5 is "
				+ rm.factorial_r(5));
		GUIKeyboard.display("Using Iteration, Factorial 5 is "
				+ rm.factorial_i(5));
	}

}
