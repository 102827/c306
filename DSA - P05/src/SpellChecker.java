import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class SpellChecker {
	private String[] wordList;

	public SpellChecker() {
		// do nothing
	}

	public SpellChecker(String filename) {
		try {
			Scanner scanner = new Scanner(new File("my.txt"));
			loadDictionary(scanner);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean isCorrectWord(String w) {
		// return slowFindWord(w);
		// return slowerFindWord(w);
		//return slowestFindWord(w);
		return binarySearchWord(w, 0, wordList.length - 1);
	}

	private boolean binarySearchWord(String word, int front, int back) {
		if (front > back) {
			System.out.println();
			return false;
		} else {
			int mid = (front + back) / 2;
			System.out.println("Word : "+word+" WordList : "+wordList[mid]+" compareTo : "+word.compareTo(wordList[mid]));
			if (word.compareTo(wordList[mid]) > 0) {
				return binarySearchWord(word, mid + 1, back);
			} else if (word.compareTo(wordList[mid]) < 0) {
				return binarySearchWord(word, front, mid - 1);
			} else {
				System.out.println();
				return true;
			}
		}
	}

	public boolean slowFindWord(String word) {
		boolean result = false;
		int idx = 0;
		while (idx < wordList.length && word.compareTo(wordList[idx]) > 0) {
			idx++;
		}

		if (idx == wordList.length) {
			return false;
		}

		if (word.equals(wordList[idx])) {
			result = true;
		}

		return result;
	}

	public boolean slowerFindWord(String word) {
		boolean result = false;
		for (String w : wordList) {
			if (w.equals(word)) {
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean slowestFindWord(String word) {
		boolean result = false;
		for (String w : wordList) {
			if (w.equals(word)) {
				result = true;
			}
		}
		return result;
	}

	public int getNumOfWords() {
		return wordList.length;
	}

	public void loadDictionary(Scanner scanner) {
		ArrayList<String> words = new ArrayList<String>();

		while (scanner.hasNextLine()) {
			String word = scanner.nextLine();
			words.add(word);
		}

		wordList = new String[words.size()];
		words.toArray(wordList);
	}

}
