public class WordCompare {

	public static void main(String[] args) {
		while (true) {
			String word1 = GUIKeyboard.readString("Enter 1st word ");
			String word2 = GUIKeyboard.readString("Enter 2nd word ");
			GUIKeyboard.display("Comparing 1st word with 2nd word = "
					+ word1.compareTo(word2));
		}
	}

}
