public class excercise {
	public static int bunnyEars(int bunnies) {
		if (bunnies > 0) {
			return 2 + bunnyEars(bunnies - 1);
		} else {
			return 0;
		}

	}

	static int[] abc1 = { 1, 11, 111, 11, 1, 11, 111, 11, 1 };
	static int[] abc2 = { 1, 2, 11 };
	static int[] abc3 = { 11, 11 };
	static int[] abc4 = { 1, 2, 3, 4 };

	public static int array11(int[] nums, int index) {
		if (index < nums.length) {
			if (nums[index] == 11) {
				return 1 + array11(nums, index + 1);
			}
			return array11(nums, index + 1);
		}
		return 0;
	}

	public static String writeSequence(int n) {
		if (n == 1) {
			return " " + n + writeSequence(n - 1) + " " + n;
		} else if (n < 0) {
			return "";
		} else {
			return "";
		}
	}

	public static void main(String[] args) {
		System.out.print(writeSequence(2));
	}

}
