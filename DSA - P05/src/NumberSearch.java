public class NumberSearch {

	public int[] sortList = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26,
			28, 30 };

	public boolean binarySearch(int searchFor, int start, int end) {
		if(start > end)
		{
			return false;
		}
		else
		{
			int mid = (start+end)/2;
			if(searchFor > sortList[mid])
			{
				return binarySearch(searchFor,mid+1,end);
			}
			else if(searchFor < sortList[mid])
			{
				return binarySearch(searchFor,start,mid-1);
			}
			else
			{
				return true;
			}
		}
	}

	public boolean isNumberInList(int num) {
		return binarySearch(num, 0, sortList.length - 1);
	}

	public static void main(String[] args) {
		NumberSearch searcher = new NumberSearch();

		for (int i = 0; i < 32; i++) {
			if (searcher.isNumberInList(i)) {
				System.out.println(i + " is found");
			} else {
				System.out.println(i + " is NOT found");
			}

		}
	}

}
