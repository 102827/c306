import java.util.ArrayList;

public class WordPuzzle {

	final int MAXWORDSIZE = 9;

	char[][] puzzle = { { 'J', 'U', 'N', 'E', 'M', 'B', 'O', 'F', 'S' },
						{ 'F', 'A', 'P', 'R', 'I', 'L', 'E', 'E', 'E' },
						{ 'A', 'C', 'R', 'J', 'U', 'L', 'Y', 'B', 'P' },
						{ 'N', 'O', 'C', 'T', 'O', 'B', 'E', 'R', 'T' },
						{ 'U', 'M', 'A', 'R', 'C', 'H', 'U', 'U', 'E' },
						{ 'A', 'U', 'G', 'U', 'S', 'T', 'R', 'A', 'M' },
						{ 'D', 'E', 'C', 'E', 'M', 'B', 'E', 'R', 'B' },
						{ 'O', 'J', 'A', 'N', 'U', 'A', 'R', 'Y', 'E' },
						{ 'Y', 'N', 'O', 'V', 'E', 'M', 'B', 'E', 'R' }};

	public ArrayList<String> generateWordList() {
		ArrayList<String> list = new ArrayList<String>();

		buildHorizontalWords(list);

		buildVerticalWords(list);

		return list;
	}

	public void buildVerticalWords(ArrayList<String> list) {
		for (int col = 0; col < puzzle[0].length; col++) {
			for (int wordsize = 4; wordsize <= MAXWORDSIZE; wordsize++) {
				for (int row = 0; row <= puzzle.length - wordsize; row++) {
					char[] letters = new char[wordsize];
					int idx = 0;
					while (idx < wordsize) {
						letters[idx] = puzzle[row + idx][col];
						idx++;
					} // while
					String word = new String(letters);
					list.add(word);
				} // for
			} // for
		} // for
	}

	public void buildHorizontalWords(ArrayList<String> list) {
		for (int row = 0; row < puzzle.length; row++) {
			for (int wordsize = 4; wordsize <= MAXWORDSIZE; wordsize++) {
				for (int col = 0; col <= puzzle[row].length - wordsize; col++) {
					String word = new String(puzzle[row], col, wordsize);
					list.add(word);
				} // for
			} // for
		} // for
	}

}
