public class SolverMain {

	public static void main(String[] args) {
		SpellChecker sp = new SpellChecker("my.txt");
		WordPuzzle wp = new WordPuzzle();

		for (String word : wp.generateWordList()) {
			if (sp.isCorrectWord(word)) {
				GUIKeyboard.display("Word Found -> " + word);
			}
		}

	}

}
