import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;


public class SpellCheckerTest {
	private SpellChecker checker;

	@Before
	public void setup() {
		String data = "aaa\nbb\ncccc";
		Scanner scanner = new Scanner(data);

		checker = new SpellChecker();
		checker.loadDictionary(scanner);
	}

	@Test
	public void testLoadDictionary() {
		assertThat(checker.getNumOfWords(), is(3));
	}

	@Test
	public void testIsCorrectWord() {
		assertThat(checker.isCorrectWord("aaa"), is(true));
		assertThat(checker.isCorrectWord("bb"), is(true));
		assertThat(checker.isCorrectWord("cccc"), is(true));
		assertThat(checker.isCorrectWord("d"), is(false));
	}

	@Test
	public void testSlowFindWord() {
		assertThat(checker.slowFindWord("aaa"), is(true));
		assertThat(checker.slowFindWord("bb"), is(true));
		assertThat(checker.slowFindWord("cccc"), is(true));
		assertThat(checker.slowFindWord("d"), is(false));
	}

	@Test
	public void testSlowerFindWord() {
		assertThat(checker.slowerFindWord("aaa"), is(true));
		assertThat(checker.slowerFindWord("bb"), is(true));
		assertThat(checker.slowerFindWord("cccc"), is(true));
		assertThat(checker.slowerFindWord("d"), is(false));
	}

	@Test
	public void testSlowestFindWord() {
		assertThat(checker.slowestFindWord("aaa"), is(true));
		assertThat(checker.slowestFindWord("bb"), is(true));
		assertThat(checker.slowestFindWord("cccc"), is(true));
		assertThat(checker.slowestFindWord("d"), is(false));
	}
}
