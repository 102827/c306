import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class WordPuzzleTest {
	private WordPuzzle puzzle;

	@Before
	public void setup() {
		puzzle = new WordPuzzle();
	}

	@Test
	public void testNumHorizontalWord() {
		ArrayList<String> list = new ArrayList<String>();

		puzzle.buildHorizontalWords(list);

		assertThat(list.size(), is(189));
		assertThat(list, hasItem("JUNE"));
		assertThat(list, hasItem("STRA"));
	}

	@Test
	public void testNumVerticalWord() {
		ArrayList<String> list = new ArrayList<String>();

		puzzle.buildVerticalWords(list);

		assertThat(list.size(), is(189));
		assertThat(list, hasItem("TRUE"));
		assertThat(list, hasItem("MBER"));
	}


	@Test
	public void testNumWordList() {
		ArrayList<String> result = puzzle.generateWordList();

		assertThat(result.size(), is(189 + 189));
	}

}
