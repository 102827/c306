import java.util.ArrayList;

public class COEApp {

	public Bid findBidWithHighestPremium(ArrayList<Bid> bids) {
		
		Bid highestBid = bids.get(0);
		for(Bid loopList: bids)
		{
			if(loopList.getPremium() > highestBid.getPremium())
			{
				highestBid = loopList;
			}
		}
		return highestBid;
	}

	public Bid findBidWithLowestPremium(ArrayList<Bid> bids) {
		
		Bid lowestBid = bids.get(0);
		for(Bid loopList: bids)
		{
			if(loopList.getPremium() < lowestBid.getPremium())
			{
				lowestBid = loopList;
			}
		}
		return lowestBid;
	}

	public double calcTotalPremium(ArrayList<Bid> bids) {
		double total = 0;
		for(Bid loopList : bids)
		{
			total += loopList.getPremium();
		}
		return total;
	}
	
	public Bid findBid(ArrayList<Bid> bids, int year, String month, int round) {
		Bid search = null;
		for(Bid loopList : bids)
		{
			if((loopList.getYear() == year) && (loopList.getMonth().equalsIgnoreCase(month)) && (loopList.getRound() == round))
			{
				search = loopList;
			}
			/*########  	SOLUTION  		########
			
			 if(loopList.isThisBid(year, month, round)
			 {
			 	search = loopList;
			 	break;
			 }
			 
			 */
		}
		return search;
	}
	
	public ArrayList<Bid> findGreatestDifferenceInPremium(ArrayList<Bid> bids) {
		
		ArrayList<Bid> difference = new ArrayList<Bid>();
		
		Bid prev = bids.get(0);
		double cD = 0.0;
		double gD = 0.0;
		for(Bid loopList:bids)
		{
			if(loopList.getYear() == prev.getYear() && loopList.getMonth().equalsIgnoreCase(prev.getMonth()) )
			{
				System.out.println("test");
				cD = loopList.getPremium() - prev.getPremium();
				if(cD < 0)
				{
					cD *= -1;
				}
				if(cD > gD)
				{
					gD = cD;
					difference.removeAll(bids);
					difference.add(loopList);
					difference.add(prev);
					prev = loopList;
				}
				else
				{
					prev = loopList;
				}
			}
			else
			{
				prev = loopList;
			}
		}
		/*Bid prev = bids.get(0);
		
		Double currentDifference = 0.0;
		Double greatestDifference = 0.0;
		
		for(Bid loopList : bids)
		{
			currentDifference = loopList.getPremium() - prev.getPremium();
			if(currentDifference < 0)
			{
				currentDifference *= -1;
			}
			if(currentDifference > greatestDifference)
			{
				greatestDifference = currentDifference;
				difference.removeAll(bids);
				difference.add(loopList);
				difference.add(prev);
				prev = loopList;
			}
			else
			{
				prev = loopList;
			}
		}*/
		return difference;
	}
	
	public ArrayList<String[]> averageEachMonth(ArrayList<Bid> bids){
		
		
		Bid prev = bids.get(0);
		double cA = 0.0;
		
		ArrayList<String[]> cde = new ArrayList<String[]>();
		for(Bid loopList:bids)
		{
			if(loopList.getYear() == prev.getYear() && loopList.getMonth().equalsIgnoreCase(prev.getMonth()) )
			{
				cA = (prev.getPremium() + loopList.getPremium()) /2;
				
				String average = Double.toString(cA);
				String year    = Integer.toString(loopList.getYear());
				String month   = loopList.getMonth();
				
				String[] abc = {average,year,month};
				cde.add(abc);
				prev = loopList;
			}
			else
			{
				prev = loopList;
			}
		}
		
		return cde;
		
	}
	
	
public ArrayList<String[]> averageForMonth(ArrayList<Bid> bids){
		ArrayList<String[]> laterList = new ArrayList<String[]>();
	
		String[] months = {"January", "February", "March", "April", "May", "June","July","August","September","October","November","December"};
		for (int i=0; i<months.length;i++)
		{
			ArrayList<Bid> newList = new ArrayList<Bid>();
			for(Bid thisMonth: bids)
			{
				if(thisMonth.getMonth().equalsIgnoreCase(months[i]))
				{
					newList.add(thisMonth);
				}
			}
			String average = Double.toString(calcTotalPremium(newList)/newList.size());
			String month = months[i];
			
			String[] data = {average, month};
			laterList.add(data);
		}
	
		return laterList;
		
	}
	
	public ArrayList<Bid> filterBidsByMonth(ArrayList<Bid> bids, String month) {
		ArrayList<Bid> filter = new ArrayList<Bid>();
		for(Bid loopList : bids)
		{
			if(loopList.getMonth().equalsIgnoreCase(month))
			{
				filter.add(loopList);
			}
		}
		return filter;
	}
}
