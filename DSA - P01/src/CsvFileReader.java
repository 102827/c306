import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class CsvFileReader {
	public ArrayList<Bid> loadBids(String filename) {
		ArrayList<Bid> result = null;
		try {
			Scanner scanner = new Scanner(new File(filename));
			result = extractBids(scanner);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public ArrayList<Bid> extractBids(Scanner scanner) {
		ArrayList<Bid> list = new ArrayList<Bid>();

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] column = line.split(",");

			list.add(createBid(column));
		}
		return list;
	}

	public Bid createBid(String[] column) {
		
		String category = "A"; 
		int year = Integer.parseInt(column[0]);
		String month = column[1];
		int round = Integer.parseInt(column[2]);
		int quota = Integer.parseInt(column[4]);
		int bids = Integer.parseInt(column[5]);
		double premium = Double.parseDouble(column[3]);
		
		Bid newBid = new Bid(category, year, month, round, quota, bids, premium);
		
		return newBid;
	}
}
