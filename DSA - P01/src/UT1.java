import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UT1 {
	public static void main(String[] args) {
		System.out.println(isPrime(51));
		System.out.println(isPrime(19));
		System.out.println(isPrime(13));
	}

	static String strArray[] = { "Mary", "john" };

	public static void try1() {
		int x = 0;
		for (String s : strArray) {
			x += s.length();
		}
		System.out.println(x);
	}

	public enum Cell {
		CROSS, BLANK, STICK;
	}

	public static void try2()
	{
		Cell grid[][] = new Cell[3][3];
		for (int i = 0; i < 3; i++)
		{
		  for (int j = 0; j < 3; j++)
		  { 
		     if(i%2 == j%2)
		     {
		    	 grid[i][j] = Cell.CROSS;
		     }
		     if(i == 1 && j == 1)
		     {
		    	 grid[i][j] = Cell.BLANK;
		     }
		     else
		     {
		    	 grid[i][j] = Cell.STICK;
		     }
		  }
		}
		for (int i = 0; i < 3; i++)
		{
		  for (int j = 0; j < 3; j++)
		  { 
		System.out.print(grid[i][j]);
		  }
		}
	}
	
	public static void try4()
	{
		JFrame frame1 = new JFrame("Border Layout");
		frame1.setBounds(50, 50, 150, 100);
		frame1.setLayout(new BorderLayout());
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton ditBtn = new JButton("DIT");
		JButton dbisBtn = new JButton("DBIS");
		JButton dbaBtn = new JButton("DBA");
		
		frame1.add(ditBtn,BorderLayout.NORTH);
		frame1.add(dbisBtn, BorderLayout.CENTER);
		frame1.add(dbaBtn,BorderLayout.SOUTH);
		
		frame1.setVisible(true);
	}
	
	public static void try3()
	{
		JFrame frame1 = new JFrame("Grid Layout");
		frame1.setBounds(50, 50, 150, 100);
		frame1.setLayout(new GridLayout(3, 1));
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton ditBtn = new JButton("DIT");
		JButton dbisBtn = new JButton("DBIS");
		JButton dbaBtn = new JButton("DBA");
		
		frame1.add(ditBtn);
		frame1.add(dbisBtn);
		frame1.add(dbaBtn);
		
		frame1.setVisible(true);
	}
	public static int[] numss = {11,2,3,51,12,5,6};
	public static void try5(){ 
	    for(int ss: numss)
	    {
	    	System.out.println(ss);
	    }

	}
	private static boolean isPrime(int number) {        
        int[] primeNumbers = {50,40,30,20,10,5,2}; 
        for(int PN : primeNumbers)
        {
            if(number%PN==0){
                return true;
            }
        }
        return false;
    }      

}
