import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class GUIMenuCOE {

	private static String[] menuItem = { "Display Average Premium",
			"Display Highest/Lowest Premium",
			"Display Highest Difference between Two Consecutive Rounds",
			"Filter By Month",
			"Display Premium for Specified Year, Month and Round",
			"Display Average Premium Each Month (In Year and Month)",
			"Display Average Premium Each Month (In Month Only)",};
	public static int counter;
	private static ArrayList<Bid> list;
	private static COEApp app;
	
	private static void init() {
		CsvFileReader cfr = new CsvFileReader();
		list = cfr.loadBids("coe.csv");

		app = new COEApp();
	}
	
	
	/* ############			DISPLAY AVERAGE			##############*/
	/* ############			CASE	   ZERO			##############*/
	private static void displayAveragePremium() {
		
		double totalPremium = app.calcTotalPremium(list);
		double averagePremium = totalPremium/list.size();
		
		GUIMenuCOE.output("D I S P L A Y   A V E R A G E   C O E   P R E M I U M");
		GUIMenuCOE.output("Average COE Premium is " + averagePremium);
		GUIMenuCOE.output(" ");
	}
	
	
	/* ############			DISPLAY HIGHEST			##############*/
	/* ############			CASE	    ONE			##############*/
	private static void displayHighestPremium() {
		Bid maxBid = app.findBidWithHighestPremium(list);

		GUIMenuCOE.output("D I S P L A Y   H I G H E S T   C O E   P R E M I U M");
		if (maxBid == null) 
		{
			GUIMenuCOE.output("No Max Bid");
		} 
		else 
		{
			GUIMenuCOE.output("Highest COE preimum : " + maxBid.getPremium()) ;
		}
		GUIMenuCOE.output(" ");
	}
	
	
	/* ############			DISPLAY LOWEST			##############*/
	/* ############			CASE       ONE			##############*/
	private static void displayLowestPremium() {
		Bid minBid = app.findBidWithLowestPremium(list);

		GUIMenuCOE.output("D I S P L A Y   L O W E S T   C O E   P R E M I U M");
		if (minBid == null) 
		{
			GUIMenuCOE.output("No Min Bid");
		} 
		else 
		{
			GUIMenuCOE.output("Lowest COE preimum : " + minBid.getPremium());
		}
		GUIMenuCOE.output(" ");
	}
	
	
	/* ############		   DISPLAY DIFFERENCE		##############*/
	/* ############			CASE       TWO			##############*/
	private static void displayDifference() {
		ArrayList<Bid> arrayBid = app.findGreatestDifferenceInPremium(list);

		GUIMenuCOE.output("D I S P L A Y   D I F F E R E N C E   I N   C O E   P R E M I U M");
		if (arrayBid == null) 
		{
			GUIMenuCOE.output("Error..");
		} 
		else 
		{
			if(arrayBid.get(0).getPremium() > arrayBid.get(1).getPremium())
			{
				GUIMenuCOE.output("Highest COE Premium : "+arrayBid.get(0).getPremium() + "  |   Month : "+arrayBid.get(0).getMonth()+"  |   Year : "+arrayBid.get(0).getYear());
				GUIMenuCOE.output("Lowest COE Premium  : "+arrayBid.get(1).getPremium() + "  |   Month : "+arrayBid.get(1).getMonth()+"  |   Year : "+arrayBid.get(1).getYear());
				GUIMenuCOE.output("Difference          : "+(arrayBid.get(0).getPremium() - arrayBid.get(1).getPremium()) );
			}
			else
			{
				GUIMenuCOE.output("Highest COE Premium : "+arrayBid.get(1).getPremium()+ "  |   Month : "+arrayBid.get(1).getMonth()+"  |   Year : "+arrayBid.get(1).getYear());
				GUIMenuCOE.output("Lowest COE Premium  : "+arrayBid.get(0).getPremium()+ "  |   Month : "+arrayBid.get(0).getMonth()+"  |   Year : "+arrayBid.get(0).getYear());
				GUIMenuCOE.output("Difference          : "+(arrayBid.get(1).getPremium() - arrayBid.get(0).getPremium()));
			}
		}
		GUIMenuCOE.output(" ");
	}
	
	
	/* ############		 	 FILTER BY MONTH	##############*/
	/* ############			CASE       THREE			##############*/
	private static void filterByMonth(String month) {
		ArrayList<Bid> Months = app.filterBidsByMonth(list, month);

		GUIMenuCOE.output("F I L T E R   B Y   M O N T H");
		if (Months == null) 
		{
			GUIMenuCOE.output("ERROR....");
		} 
		else 
		{
			for(Bid what: Months)
			{
				GUIMenuCOE
				.output("Year: "+what.getYear() + " Month: "+what.getMonth()+" Round: "+what.getRound()+" Premium: "+what.getPremium()+" Quota: "+what.getQuota()+" Bidders: "+what.getBidders() );
			}
		}
		GUIMenuCOE.output(" ");
	}
	
	
	/* ############			SEARCH PREMIUM			##############*/
	/* ############			CASE      FOUR			##############*/
	private static void displayPremium(int year, String month, int round) {
		Bid bid = app.findBid(list, year, month, round);

		GUIMenuCOE.output("S E A R C H   P R E M I U M");
		if (bid == null) 
		{
			GUIMenuCOE.output("No Bid found.");
		} 
		else 
		{
			GUIMenuCOE
			.output("Year: "+bid.getYear() + " Month: "+bid.getMonth()+" Round: "+bid.getRound()+" Premium: "+bid.getPremium()+" Quota: "+bid.getQuota()+" Bidders: "+bid.getBidders() );
		}
		GUIMenuCOE.output(" ");
	}
	
	
	private static void displayAvg(){
		ArrayList<String[]> bid = app.averageEachMonth(list);
		for(String[] what : bid)
		{
			GUIMenuCOE
			.output("Year : "+what[1]+" Month : "+what[2]+" Average : "+what[0]);
		}
		
	}
	private static void displayAvgM(){
		ArrayList<String[]> bid = app.averageForMonth(list);
		for(String[] what : bid)
		{
			GUIMenuCOE
			.output("Month : "+what[1]+" Average : "+what[0]);
		}
	}
	
	/* ############			DO 		OPTION			##############*/
	private static void doOption(int choice) {
		switch (choice) {
		case 0:
			displayAveragePremium();
			break;

		case 1:
			counter++;
			if(counter%2 == 0)
			{
				displayLowestPremium();
			}
			else
			{
				displayHighestPremium();
			}
			break;
		case 2:
			displayDifference();
			break;
		case 3:
			String month = GUIKeyboard.readString("Enter Month ");
			filterByMonth(month);
			break;
		case 4:
			int year = GUIKeyboard.readInt("Enter Year ");
			String month2 = GUIKeyboard.readString("Enter Month ");
			int round = GUIKeyboard.readInt("Enter Round ");
			displayPremium(year, month2, round);
			break;
		case 5:
			displayAvg();
			break;
		case 6:
			displayAvgM();
			break;
		}
	}

	public static void main(String[] args) {
		GUIMenuCOE.showMenu("COE Application");
	}

	//
	// DO NOT CHANGE ANY CODE FROM THIS POINT ONWARDS
	// UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
	//
	private static JFrame win;
	private static DefaultListModel listModel = new DefaultListModel();
	private static JList jlist = new JList(listModel);

	public static void showMenu(String title) {
		win = new JFrame(title);
		win.setBounds(100, 100, 600, 400);

		// Create the Menu Option Buttons
		JPanel p1 = new JPanel(new GridLayout(menuItem.length, 1, 5, 5));
		p1.setBorder(new TitledBorder("Menu Items"));
		for (int i = 0; i < menuItem.length; i++) {
			JButton btn = new JButton(menuItem[i]);
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String command = e.getActionCommand();
					for (int i = 0; i < menuItem.length; i++) {
						if (command.equals(menuItem[i])) {
							doOption(i);
							break;
						}
					}
				}
			});
			p1.add(btn);
		}
		win.add(p1, BorderLayout.NORTH);

		// Create the Display Area
		JPanel p2 = new JPanel(new BorderLayout());
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));
		JScrollPane scrollPane = new JScrollPane(jlist);
		p2.setBorder(new TitledBorder("Display Area"));
		p2.add(scrollPane, BorderLayout.CENTER);
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));

		JPanel p3 = new JPanel(new FlowLayout());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
			}
		});

		p3.add(btnClear);
		p2.add(p3, BorderLayout.SOUTH);
		win.add(p2, BorderLayout.CENTER);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);

		init();
	}

	public static void output(String line) {
		String[] lines = line.split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}
}
