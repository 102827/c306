import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GUIMenuMysteryNumber {

	JTextField textField1 = new JTextField("Type a number here");
	final JLabel label2 = new JLabel("Start Guessing");
	final JLabel label3 = new JLabel("The number between 1 & 49");
	MysteryNumber num;

	public static void main(String[] args) {
		new GUIMenuMysteryNumber();
	}

	public GUIMenuMysteryNumber() {

		JFrame frame1 = new JFrame("Border Layout");
		frame1.setBounds(50, 150, 500, 300);
		frame1.setLayout(new BorderLayout());
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel topPanel = new JPanel();
		final JLabel label1 = new JLabel("Guess a number between 1 to 49");

		label1.setForeground(Color.BLUE);
		topPanel.setBackground(Color.WHITE);
		topPanel.add(label1);

		JPanel centerPanel = new JPanel();
		label2.setFont(new Font("Serrif", Font.PLAIN, 50));
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label3.setHorizontalAlignment(SwingConstants.CENTER);
		centerPanel.setLayout(new BorderLayout());
		centerPanel.setBackground(Color.YELLOW);
		centerPanel.add(label2, BorderLayout.CENTER);
		centerPanel.add(label3, BorderLayout.SOUTH);

		JPanel bottomPanel = new JPanel();
		JButton button1 = new JButton("Generate Number");
		JButton button2 = new JButton("Guess");
		textField1.setColumns(10);
		bottomPanel.setLayout(new FlowLayout());
		bottomPanel.add(button1);
		bottomPanel.add(textField1);
		bottomPanel.add(button2);

		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				genNumber();
			}

		});

		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				guessNumber();
			}
		});

		frame1.add(topPanel, BorderLayout.NORTH);
		frame1.add(centerPanel, BorderLayout.CENTER);
		frame1.add(bottomPanel, BorderLayout.SOUTH);
		frame1.setVisible(true);
	}

	private void genNumber() {
		num = new MysteryNumber();
		label2.setText("Start Guessing!");
		label3.setText("");
		textField1.setText("");
	}

	private void guessNumber() {
		if(!num.isFieldEmpty(textField1.getText()))
		{
			int guessNumber = Integer.parseInt(textField1.getText());
			label2.setText(num.getResult(guessNumber).toString());
			label3.setText(num.getHint(guessNumber));
		}
		else
		{
			label2.setText("PLEASE ENTER A NUMBER");
		}
	}

}
