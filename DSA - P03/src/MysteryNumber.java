import java.util.Random;

public class MysteryNumber {
	private int high = 49;
	private int low = 1;
	private int mysteryNumber;

	public MysteryNumber() {
		mysteryNumber = new Random().nextInt(50);
	}

	public int getNumber() {
		return mysteryNumber;
	}

	public Result getResult(int guessNumber) {
		if (guessNumber == mysteryNumber) {
			return Result.EQUAL;
		} else if (guessNumber > mysteryNumber) {
			return Result.HIGH;
		} else {
			return Result.LOW;
		}
	}

	public String getHint(int guessNumber) {
		if (guessNumber == mysteryNumber) {
			return "";
		} else if (guessNumber > mysteryNumber) {
			if (guessNumber > high) {
				return "Please Enter A Number Between " + low + " and " + high;
			} else {
				high = guessNumber - 1;
				return "The number is between " + low + " and "
						+ (guessNumber - 1);
			}
		} else {
			if (guessNumber < low) {
				return "Please Enter A Number Between " + low + " and " + high;
			} else {
				low = guessNumber + 1;
				return "The number is between " + (guessNumber + 1) + " and "
						+ high;
			}
		}
	}
	
	public boolean isFieldEmpty(String guessNumber)
	{
		if(guessNumber.length() == 0)
		{
			return true;
		}
		return false;
	}
}
