import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LayoutDemo {

	public LayoutDemo() {

		JFrame frame1 = new JFrame("Border Layout");
		frame1.setBounds(50, 50, 200, 200);
		frame1.setLayout(new BorderLayout());
		frame1.add(new JButton("Button A"), BorderLayout.NORTH);
		frame1.add(new JButton("Button B"), BorderLayout.SOUTH);
		frame1.add(new JButton("Button C"), BorderLayout.EAST);
		frame1.add(new JButton("Button D"), BorderLayout.WEST);
		frame1.add(new JButton("Button E"), BorderLayout.CENTER);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setVisible(true);

		JFrame frame2 = new JFrame("Flow Layout");
		frame2.setBounds(250, 250, 200, 200);
		frame2.setLayout(new FlowLayout());
		frame2.add(new JButton("Button 1"));
		frame2.add(new JButton("Button 2"));
		frame2.add(new JButton("Button 3"));
		frame2.add(new JButton("Button 4"));
		frame2.add(new JButton("Button 5"));
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.setVisible(true);

		JFrame frame3 = new JFrame("Combined Layout");
		frame3.setBounds(450, 450, 400, 400);
		frame3.setLayout(new BorderLayout());

		JPanel topPanel = new JPanel();
		topPanel.add(new JLabel("LabelA"));
		topPanel.add(new JLabel("LabelB"));
		topPanel.add(new JLabel("LabelC"));

		JPanel bottomPanel = new JPanel();
		bottomPanel.add(new JButton("Button 2"));
		bottomPanel.add(new JButton("Button 1"));

		JPanel centerPanel = new JPanel();
		centerPanel.add(new JTextField("Hello"));

		frame3.add(topPanel, BorderLayout.NORTH);
		frame3.add(bottomPanel, BorderLayout.SOUTH);
		frame3.add(new JButton("Button C"), BorderLayout.EAST);
		frame3.add(new JButton("Button D"), BorderLayout.WEST);
		frame3.add(centerPanel, BorderLayout.CENTER);

		frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame3.setVisible(true);

	}

	public static void main(String[] args) {
		new LayoutDemo();
	}

}
